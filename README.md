# Dice of Fate Project

## Technologies

* [Unity v2022.3.4f1](https://unity.com/)
* [Ink](https://www.inklestudios.com/ink/)

`Please Donwload the version 2022.3.4f1, if anybody uses any upper version the changes will be deleted, there is no version rollback function in unity`

## How to contribute

No commits can be done to the master branch, unless they are small changes or minor configurations.

Every team member has to create a feature branch, the prefered name is `feature/name-of-the-feature`, this feature branch correspons to just one feature, when the feature is done and the branch is merged it will be *deleted*. You will have to create one branch per feature you are working with.

When you are ready to submit your changes to master, create a Merge Request, describe what you did and post some evidence of it working. Then send it for peer review. If there are any problems with the MR, you will have to fix them and send them again for review. If for any insance the master branch has conflicts with your you must pull from that branch into your branch. I recomend using rebase but merge is also valid.