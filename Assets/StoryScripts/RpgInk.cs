﻿using System;
using Ink.Runtime;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace StoryScripts
{
    public class RpgInk : MonoBehaviour
    {
        public static event Action<Story> OnCreateStory;

        [SerializeField] private UIControllers.StoryUIController storyUIController;

        [SerializeField] private TextAsset inkJSONAsset = null;
        
        public Story story;
        
        // UI Prefabs

        private void Awake()
        {
           StartStory();
        }

        void StartStory()
        {
            story = new Story(inkJSONAsset.text);
            if (OnCreateStory != null) OnCreateStory(story);
            storyUIController.OpenStoryUI();
            RefreshView();
            storyUIController.onChoiceSelected.AddListener(OnChoiceSelected);
        }

        private void OnDestroy()
        {
            storyUIController.onChoiceSelected.RemoveAllListeners();
        }

        void RefreshView()
        {
            if (!story.canContinue)
            {
                if (story.currentChoices.Count > 0)
                {
                    // Display current Choices
                    storyUIController.OpenChoices(story.currentChoices);
                    return;
                }
                // IDK
                storyUIController.CloseStoryUI();
                return;
            }
            
            string text = story.Continue().Trim();
            UpdateContentView(text);
        }

        public void ContinueStory()
        {
            // First we need to check if the story can continue
            // TODO
            RefreshView();
        }

        private void OnChoiceSelected(int index)
        {
            story.ChooseChoiceIndex(index);
            ContinueStory();
        }

        void UpdateContentView(string text)
        {
            storyUIController.UpdateText(text);
        }
    }
}