﻿using System;
using Ink.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIControllers
{
    public class ChoiceButton : MonoBehaviour
    {
        public UnityEvent<int> onChoiceSelected;

        [SerializeField] private TMP_Text buttonText;

        private Button _button;

        private Choice _choice;

        public Choice buttonsChoice
        {
            set
            {
                _choice = value;
                buttonText.text = _choice.text;
            }
        }

        private void Start()
        {
            _button = GetComponent<Button>();
            if (_button == null) return;
            _button.onClick.AddListener(OnClicked);
        }

        private void OnClicked()
        {
            onChoiceSelected.Invoke(_choice.index);
        }
    }
}