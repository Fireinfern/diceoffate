﻿using System;
using System.Collections.Generic;
using Ink.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace UIControllers
{
    [RequireComponent(typeof(Canvas))]
    public class StoryUIController : MonoBehaviour
    {
        [SerializeField] private TMP_Text storyTextComponent;

        [SerializeField] private ChoiceButton choiceButtonPrefab;

        [SerializeField] private Canvas secondaryCanvas;

        [SerializeField] private GameObject choicesContainer;
        
        private Canvas _storyCanvas;

        public UnityEvent<int> onChoiceSelected;

        private void Start()
        {
            _storyCanvas = GetComponent<Canvas>();
        }

        public void OpenStoryUI()
        {
            
        }

        public void CloseStoryUI()
        {
            
        }

        public void OpenChoices(List<Choice> choices)
        {
            // TODO add animations
            secondaryCanvas.gameObject.SetActive(true);
            foreach (var choice in choices)
            {
                var newChoiceButton = Instantiate(choiceButtonPrefab.gameObject, choicesContainer.transform);
                var choiceButton = newChoiceButton.GetComponent<ChoiceButton>();
                choiceButton.buttonsChoice = choice;
                choiceButton.onChoiceSelected.AddListener(OnChoiceSelected);
            }
        }

        public void CloseChoices()
        {
            // TODO add animations
            secondaryCanvas.gameObject.SetActive(false);
            foreach (Transform child in choicesContainer.transform)
            {
                child.GetComponent<ChoiceButton>()?.onChoiceSelected.RemoveAllListeners();
                Destroy(child.gameObject);
            }
        }

        public void OnChoiceSelected(int index)
        {
            CloseChoices();
            onChoiceSelected.Invoke(index);
        }
        
        public void UpdateText(string newText)
        {
            storyTextComponent.text = newText;
            
        }
    }
}